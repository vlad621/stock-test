import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { Stock } from '../model/stock';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  url = 'https://api.iextrading.com/1.0/tops/last?symbols='
  constructor(private http: HttpClient) {}
  getStocks(symbols): Observable<HttpResponse<Stock[]>> {
    return this.http.get<Stock[]>(
      this.url + symbols,
      { observe: 'response' }
    );
  }
}