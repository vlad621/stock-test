import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { ApiService } from '../service/api.service';
import { Stock } from '../model/stock';

import { Sort } from '@angular/material/sort';

import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.scss']
})
export class StockComponent implements OnInit {

  stocks: Stock[] = [];

  displayedColumns: string[] = ['symbol', 'price', 'size', 'time', 'action']
  dataSource = new MatTableDataSource(this.stocks);
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private api: ApiService) { }

  ngOnInit() {
    // this.getStocks("SNAP, MSFT, FB, C, AIG, AMZN, BAC, AAPL, GE");
    this.dataSource.sort = this.sort;
  }

  updateTableDataSource() {
    this.dataSource.data = this.stocks;
  }

  getStocks(symbols) {
    this.api.getStocks(symbols)
      .subscribe(resp => {
        for (const data of resp.body) {
          this.stocks.push(data);
        }
        this.updateTableDataSource();
      });
  }

  sortStocks(sort: Sort) {
    const data = this.stocks.slice();
    if (!sort.active || sort.direction === '') {
      this.stocks = data;
      this.updateTableDataSource();
      return;
    }

    this.stocks = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'price': return compare(a.price, b.price, isAsc);
        default: return 0;
      }
    });
    this.updateTableDataSource();
  }

  onSubmit(form: NgForm) {
    this.getStocks(form.value.symbols);
    form.reset();
  }

  onRemove(stock: Stock) {
    this.stocks = this.stocks.filter(item => item !== stock);
    this.updateTableDataSource();
  }
}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}